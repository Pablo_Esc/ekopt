package me.pogorzelski.ekopt;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.text.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Util {

    public static final Logger logger = LogManager.getLogger(Logger.class.getName());
    private static final int BUFFER_SIZE = 4096;

    public static Boolean checkIntCon() {
        Socket sock = new Socket();
        InetSocketAddress addr = new InetSocketAddress("google.com", 80);
        try {
            sock.connect(addr, 3000);
            return true;
        } catch (IOException e) {
            return false;
        } finally {
            try {
                sock.close();
            } catch (IOException e) {
            }
        }
    }

    public static void addToLog(TextArea textArea, String string) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                textArea.appendText("\n" + dateFormat.format(cal.getTime()) + " : " + string);
            }
        });

    }

    public static void updateTime(Text text, long time) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                text.setText("Time till finish \n" + String.valueOf(time) + " seconds");
            }
        });

    }


    public static String downloadCrystark(String currentVersion) throws IOException {
        logger.info("Trying to check for new release");
    	URL url = new URL("https://bitbucket.org/Crystark/ek-battlesim/wiki/Home");
        Document doc = Jsoup.parse(url, 60 * 1000);
        logger.info("Parsing webpage");
        String downloadLink = doc.select("a[href*=deploy/downloads/]").attr("href").toString();
//        downloadLink = downloadLink.replace("/src/", "/raw/").replace("?at=releases", "");
        logger.info(downloadLink);
        String linkVersion = "0.0.0";
        Pattern linkPattern = Pattern.compile(".*-(\\d.\\d.\\d.*).zip");
        Matcher linkMatcher = linkPattern.matcher(downloadLink);
        logger.info(linkMatcher);
        while (linkMatcher.find()) {
            linkVersion = linkMatcher.group(1);
        }
        logger.info("compare");
        logger.info(currentVersion);
        logger.info(linkVersion);
        if (!currentVersion.equals(linkVersion) && !linkVersion.equals("0.0.0")) {
            Alert alertUpdate = new Alert(Alert.AlertType.INFORMATION);
            alertUpdate.setContentText("Downloading new version of Crystark " + linkVersion + ". \n Click OK and please wait.");
            alertUpdate.setHeaderText("Updating");
            alertUpdate.setTitle("Update information");
            alertUpdate.showAndWait();
            URL link = new URL(downloadLink);
            InputStream in = new BufferedInputStream(link.openStream());
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int n = 0;
            while (-1 != (n = in.read(buf))) {
                out.write(buf, 0, n);
            }
            out.close();
            in.close();
            byte[] response = out.toByteArray();

            File fileName = new File(App.dataDir, "binary.zip");
            FileOutputStream fos = new FileOutputStream(fileName);
            fos.write(response);
            fos.close();

            unzip(fileName, new File(App.dataDir));
            Files.copy(new File(new File(App.dataDir, App.binDir), "ek-battlesim.exe").toPath(), new File(new File(App.dataDir, App.binDir), "ek-battlesim.jar").toPath(), StandardCopyOption.REPLACE_EXISTING);
            alertUpdate.close();
        }
        return linkVersion;
    }

    public static void unzip(File zipFilePath, File destDirectory) throws IOException {
        File destDir = destDirectory;
        if (!destDir.exists()) {
            destDir.mkdir();
        }
        ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath));
        ZipEntry entry = zipIn.getNextEntry();
        // iterates over entries in the zip file
        while (entry != null) {
            String filePath = destDirectory + File.separator + entry.getName();
            if (!entry.isDirectory()) {
                // if the entry is a file, extracts it
                extractFile(zipIn, filePath);
            } else {
                // if the entry is a directory, make the directory
                File dir = new File(filePath);
                dir.mkdir();
            }
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
        }
        zipIn.close();
    }

    private static void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
    }

    public static void dumpToTxt(String textToDump, String filename) throws IOException {
        File outputFile = new File(new File(App.dataDir, App.binDir), filename);
        BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));
        writer.write(textToDump);
        writer.close();
    }

}
