package me.pogorzelski.ekopt;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.concurrent.Service;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.FileChooser;

public class MainCtrl implements Initializable {

	public static final Logger logger = LogManager.getLogger(Logger.class
			.getName());
	public final List<String> demonList = new ArrayList<String>();
	public final List<String> ewTargetList = new ArrayList<String>();
	public Preferences preferences = Preferences.userRoot();
	public Simulation simulation = new Simulation("0");
	public Thread th = new Thread();
	public String binaryVersion = "";
	public Boolean checkUpdate = false;

	@FXML
	public Text textEstimatedTime;
	@FXML
	public ComboBox<String> comboBoxEwTarget = new ComboBox<>();

	@FXML
	public ComboBox<String> comboBoxTargetType;
	@FXML
	public VBox vBoxTarget;
	public Text textCrystarkVersion;
	public Text textStatus;
	public TextField textFieldWaitToPlay;
	public TextField textFieldJavaPath;
	public TextField textFieldMinCards;
	public TextField textFieldCustomDemon;
	public Button buttonStopOpt;
	public CheckBox checkBoxCost;
    public Label labelSimCount;
    @FXML
	TextArea textAreaCardPool;
	@FXML
	Text textTarget;
	@FXML
	ComboBox<String> comboBoxDemonName;
	@FXML
	TextArea textAreaCardList;
	@FXML
	TextField textFieldPlayerLvl;
	@FXML
	Button buttonRunOpt;
	@FXML
	ProgressBar progressBar;
	@FXML
	Tab tabPaneCardPool;
	@FXML
	Tab tabPaneOutput;
	@FXML
	Tab tabPaneSettings;
	@FXML
	TextArea textAreaLog;
	@FXML
	TextArea textAreaOutput;
	@FXML
	CheckBox checkBoxUpdate;
	@FXML
	TextField textFieldInnerIterations;
	@FXML
	TextField textFieldRandomIterations;
	@FXML
	TextField textFieldBestIterations;
	@FXML
	TabPane tabPane;
	@FXML
	TextField textFieldRunesIterations;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			Util.addToLog(textAreaLog, "Started ekopt");
			textStatus.setText("Idle");
			File dataFile = new File(App.dataDir);
			if (!dataFile.exists() || !dataFile.isDirectory()) {
				dataFile.mkdirs();
				this.preferences.put("binary-version", "0.0.0");
				this.preferences.putBoolean("check-update", true);
			}
			loadSettings();
			if (checkUpdate) {
				logger.info("in " + binaryVersion);
				try {
					logger.info("updating binary");
					logger.info(binaryVersion);
					String newRelease = Util.downloadCrystark(binaryVersion);
					this.preferences.put("binary-version", newRelease);
					binaryVersion = newRelease;
					textCrystarkVersion.setText(newRelease);
					Util.addToLog(textAreaLog, "Crystark updated to : "
							+ newRelease);
					saveSettings();
				} catch (Exception e) {
					Util.addToLog(textAreaLog,
							"There were problems updating crystarks release");
				}
			} else {
				logger.info("Else " + this.binaryVersion);
				textCrystarkVersion.setText(this.binaryVersion);
			}
			loadDemonList();
			loadEwTargets();
			initControls();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}



	Service<Integer> simService = new Service<Integer>() {
		@Override
		protected void cancelled() {
			super.cancelled();
			Util.addToLog(textAreaLog, "Process cancelled with top value : "
					+ simulation.topMpm);
			tabPaneCardPool.disableProperty().set(false);
			tabPaneSettings.disableProperty().set(false);
			buttonRunOpt.disableProperty().set(false);
		}

		@Override
		protected void succeeded() {
			super.succeeded();
			Util.addToLog(textAreaLog, "Process end with top value : "
					+ simulation.topMpm);
			tabPaneCardPool.disableProperty().set(false);
			tabPaneSettings.disableProperty().set(false);
			buttonRunOpt.disableProperty().set(false);
		}

		@Override
		protected Task<Integer> createTask() {
			return new Task<Integer>() {
				@Override
				protected Integer call() throws Exception {

					simulation.bestDeck.output.addListener((observable,
							oldValue, newValue) -> {
						logger.info("Should update");

						Platform.runLater(() -> {
							textAreaOutput.clear();
							textAreaOutput.setText(newValue);
						});
					});

					Util.addToLog(textAreaLog, "Processing random decks");
					for (Integer iterations = 0; iterations <= Integer
							.valueOf(textFieldRandomIterations.getText()); iterations++) {
//                        labelSimCount.setText(iterations+1 + "/" + textFieldRandomIterations.getText());
						simulation.reset();
						Double bestMpm = simulation.topMpm;
						// logger.info("picking deck");
						simulation.pickDeck("random",
								textFieldMinCards.getText(),
								textFieldWaitToPlay.getText());
						// logger.info("deck picked");
						if (simulation.currentDeck.cards.size() > 0) {
							try {
								simulation.run("plain", textAreaLog);
							} catch (Exception e) {
								logger.info(e.toString());
							}
						} else {
							logger.info("Empty");
							break;
						}
						if (simulation.topMpm > bestMpm) {
							Util.addToLog(textAreaLog,
									"Found a better deck with : "
											+ simulation.topMpm);
						}
						updateProgress(iterations,
								Integer.valueOf(textFieldRandomIterations
										.getText()));
						if (Thread.currentThread().isInterrupted()) {
							Thread.currentThread().interrupt();
							return null;
						}
					}
					updateProgress(0, 100);

					Util.addToLog(textAreaLog, "Processing best decks");
					for (Integer iterations = 0; iterations <= Integer
							.valueOf(textFieldBestIterations.getText()); iterations++) {
//                        labelSimCount.setText(iterations+1 + "/" + textFieldBestIterations.getText());

						Double bestMpm = simulation.topMpm;
						simulation.reset();
						simulation.pickDeck("best",
								textFieldMinCards.getText(),
								textFieldWaitToPlay.getText());
						if (simulation.currentDeck.cards.size() > 0) {
							simulation.run("plain", textAreaLog);
						} else {
							break;
						}
						if (simulation.topMpm > bestMpm) {
							Util.addToLog(textAreaLog,
									"Found a better deck with : "
											+ simulation.topMpm);
						}
						updateProgress(iterations,
								Integer.valueOf(textFieldBestIterations
										.getText()));
						if (Thread.currentThread().isInterrupted()) {
							return null;
						}

					}
					updateProgress(0, 100);

					Util.addToLog(textAreaLog, "Finding best runes");
					for (Integer iterations = 0; iterations <= Integer
							.valueOf(textFieldRunesIterations.getText()); iterations++) {
//                        labelSimCount.setText(iterations+1 + "/" + textFieldRunesIterations.getText());
						Double bestMpm = simulation.topMpm;
						simulation.reset();
						simulation.pickDeck("runes",
								textFieldMinCards.getText(),
								textFieldWaitToPlay.getText());
						if (simulation.currentDeck.cards.size() > 0) {
							simulation.run("plain", textAreaLog);
						} else {
							break;
						}
						if (simulation.topMpm > bestMpm) {
							Util.addToLog(textAreaLog,
									"Found a better deck with : "
											+ simulation.topMpm);
						}
						updateProgress(iterations,
								Integer.valueOf(textFieldRunesIterations
										.getText()));
						if (Thread.currentThread().isInterrupted()) {
							return null;
						}
					}
					textStatus.setText("Idle");
					return null;
				}
			};
		}
	};

	public void initControls() {
		// Platform.setImplicitExit(false);
		progressBar.visibleProperty().set(false);
		progressBar.setProgress(0);
		buttonRunOpt.disableProperty().setValue(true);
		buttonStopOpt.disableProperty().setValue(true);
		textAreaLog.setPrefRowCount(3);
		comboBoxTargetType.getItems().add("Demon");
		comboBoxTargetType.getItems().add("Elemental Wars");
		comboBoxTargetType.getItems().add("Hydra");
		comboBoxTargetType.getItems().add("Thief");
		comboBoxTargetType.getItems().add("Kingdom Wars");
		comboBoxTargetType.getItems().add("Arena");
		comboBoxDemonName = new ComboBox<String>();
		comboBoxTargetType
				.valueProperty()
				.addListener(
						(observable, oldValue, newValue) -> {
							vBoxTarget.getChildren().clear();
							if (newValue.equals("Demon")) {
								textTarget = new Text();
								textTarget.setText("Select the demon");
								comboBoxDemonName.setItems(FXCollections
										.observableList(this.demonList));
								comboBoxDemonName.getItems().add("Custom");
								vBoxTarget.getChildren().add(textTarget);
								vBoxTarget.getChildren().add(comboBoxDemonName);
							} else if (newValue.equals("Elemental Wars")) {
								textTarget = new Text();
								textTarget.setText("Select the demon");
								comboBoxEwTarget.setItems(FXCollections
										.observableList(this.ewTargetList));
								vBoxTarget.getChildren().add(textTarget);
								vBoxTarget.getChildren().add(comboBoxEwTarget);
							}
							buttonRunOpt.disableProperty().setValue(false);
						});
		textFieldCustomDemon = new TextField();
		comboBoxDemonName.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> arg0, String oldVal, String newVal) {
				if (newVal.equals("Custom")) {
					vBoxTarget.getChildren().add(textFieldCustomDemon);
				} else {
					vBoxTarget.getChildren().remove(textFieldCustomDemon);
				}
				
			}
		});
	}

	
	
	public void loadCardPool() throws IOException {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Load Card Pool");
		File outputFile = fileChooser.showOpenDialog(App.currentStage);
		if (outputFile != null) {
			BufferedReader reader = new BufferedReader(new FileReader(
					outputFile));
			String line = "";
			StringBuilder inputData = new StringBuilder();
			while ((line = reader.readLine()) != null) {
				inputData.append(line + System.getProperty("line.separator"));
			}
			textAreaCardPool.setText(inputData.toString());
			reader.close();
			Util.addToLog(textAreaLog, "Card Pool loaded");
		}
	}

	public void saveCardPool() throws IOException {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Save Card Pool");
		File outputFile = fileChooser.showSaveDialog(App.currentStage);
		if (outputFile != null) {
			BufferedWriter writer = new BufferedWriter(new FileWriter(
					outputFile));
			writer.write(textAreaCardPool.getText());
			writer.close();
			Util.addToLog(textAreaLog, "Card Pool saved");
		}
	}

	public void saveOutput() throws IOException {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Save Output");
		File outputFile = fileChooser.showSaveDialog(App.currentStage);
		if (outputFile != null) {
			BufferedWriter writer = new BufferedWriter(new FileWriter(
					outputFile));
			writer.write(textAreaOutput.getText());
			writer.close();
			Util.addToLog(textAreaLog, "Output saved");
		}
	}

	public void saveSettings() throws IOException, BackingStoreException {
		this.preferences.put("player-lvl", textFieldPlayerLvl.getText());
		this.preferences.put("iter-inner", textFieldInnerIterations.getText());
		this.preferences.put("wait-to-play", textFieldWaitToPlay.getText());
		this.preferences
				.put("iter-random", textFieldRandomIterations.getText());
		this.preferences.put("iter-best", textFieldBestIterations.getText());
		this.preferences.put("iter-runes", textFieldRunesIterations.getText());
		this.preferences.put("java-path", textFieldJavaPath.getText());
		this.preferences.put("min-cards", textFieldMinCards.getText());
		this.preferences
				.putBoolean("check-update", checkBoxUpdate.isSelected());
		this.preferences.putBoolean("check-cost", checkBoxCost.isSelected());
		File settingsFile = new File("settings.xml");
		FileOutputStream fileOutputStream = new FileOutputStream(settingsFile);
		this.preferences.exportSubtree(fileOutputStream);
		fileOutputStream.close();
		Util.addToLog(textAreaLog, "Settings saved");
	}

	public void loadSettings() throws IOException, BackingStoreException {
		try {
			File settingsFile = new File("settings.xml");
			FileInputStream fileInputStream = new FileInputStream(settingsFile);
			this.preferences.importPreferences(fileInputStream);
			fileInputStream.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
		textFieldPlayerLvl.setText(this.preferences.get("player-lvl", "100"));
		textFieldRandomIterations.setText(this.preferences.get("iter-random",
				"200"));
		textFieldWaitToPlay.setText(this.preferences.get("wait-to-play", "2"));
		textFieldInnerIterations.setText(this.preferences.get("iter-inner",
				"1000"));
		textFieldBestIterations.setText(this.preferences
				.get("iter-best", "200"));
		textFieldRunesIterations.setText(this.preferences.get("iter-runes",
				"100"));
		textFieldJavaPath.setText(this.preferences.get("java-path", ""));
		textFieldMinCards.setText(this.preferences.get("min-cards", "1"));
		this.binaryVersion = this.preferences.get("binary-version", "0");
		checkUpdate = this.preferences.getBoolean("check-update", true);
		checkBoxUpdate.selectedProperty().set(
				this.preferences.getBoolean("check-update", false));
		simulation.checkCost = this.preferences.getBoolean("check-cost", true);
		checkBoxCost.selectedProperty().set(
				this.preferences.getBoolean("check-cost", true));
		simulation.innerIterations = textFieldInnerIterations.getText();
		simulation.javaPath = textFieldJavaPath.getText();
		logger.info(textFieldJavaPath.getText());
		Util.addToLog(textAreaLog, "Settings loaded");
	}

	public void loadDemonList() throws IOException {
		File cardSetFile = new File(App.dataDir, new File(App.binDir,
				"supported.cards.txt").toString());
		BufferedReader reader = new BufferedReader(new FileReader(cardSetFile));
		String line;
		while ((line = reader.readLine()) != null) {
			if (line.contains("demon")) {
				this.demonList.add(line.split(",")[0].trim());
			}
		}
		reader.close();
	}

	public void loadEwTargets() throws IOException {
		File cardSetFile = new File(App.dataDir, new File(App.binDir,
				"supported.cards.txt").toString());
		BufferedReader reader = new BufferedReader(new FileReader(cardSetFile));
		String line;
		while ((line = reader.readLine()) != null) {
			if (line.contains("(Legendary)")) {
				this.ewTargetList.add(line.split(",")[0].replace("(Legendary)", "").trim());
			}
		}
		reader.close();
	}



	public <T> void runOpt() throws IOException {
		Util.addToLog(textAreaLog, "Starting optimization process");
		textStatus.setText("Running opt");
		tabPaneCardPool.disableProperty().set(true);
		tabPaneSettings.disableProperty().set(true);
		buttonRunOpt.disableProperty().set(true);
		buttonStopOpt.disableProperty().setValue(false);
		SingleSelectionModel<Tab> singleSelectionModel = tabPane
				.getSelectionModel();
		singleSelectionModel.select(tabPaneOutput);

		simulation = new Simulation(textFieldPlayerLvl.getText());
		// logger.info(simulation.maxCost);
		simulation.innerIterations = textFieldInnerIterations.getText();
		simulation.javaPath = textFieldJavaPath.getText();
		simulation.checkCost = checkBoxCost.isSelected();
		// logger.info(comboBoxTargetType.getValue());
		if (comboBoxTargetType.getValue().equals("Demon")) {
			logger.info("Got demon");
			simulation.target = "demon";
			if (comboBoxDemonName.getValue().equals("Custom")) {
				simulation.targetValue = textFieldCustomDemon.getText();
			} else {
				simulation.targetValue = comboBoxDemonName.getValue();
			}
		} else if (comboBoxTargetType.getValue().equals("Elemental Wars")) {
			logger.info("Got ew");
			simulation.target = "ew";
			simulation.targetValue = comboBoxEwTarget.getValue();
		} else if (comboBoxTargetType.getValue().equals("Kingdom Wars")) {
			logger.info("Got kw");
			simulation.target = "kw";
		} else if (comboBoxTargetType.getValue().equals("Hydra")) {
			logger.info("Got hydra");
			simulation.target = "hydra";
		} else if (comboBoxTargetType.getValue().equals("Thief")) {
			logger.info("Got thief");
			simulation.target = "thief";
		} else if (comboBoxTargetType.getValue().equals("Arena")) {
			logger.info("Got arena");
			simulation.target = "arena";
		}

		// load data from screen
		simulation.playerLvl = textFieldPlayerLvl.getText();
		// simulation.target = "demon";
		// simulation.targetValue =
		// comboBoxDemonName.getSelectionModel().getSelectedItem();

		simulation.loadCardSet(textAreaCardPool.getText());

		// main sim thread
		progressBar.visibleProperty().set(true);
		progressBar.progressProperty().bind(simService.progressProperty());
		logger.info("Got so far");
		// th.setDaemon(true);
		// th = new Thread(simService);
		logger.info("Trying to start");
		// th.start();
		simService.reset();
		simService.start();
		logger.info("After start");
	}

	public void stopOpt(ActionEvent actionEvent) throws InterruptedException {
		Util.addToLog(textAreaLog, "Optimization stopped");
		simService.cancel();
		buttonRunOpt.disableProperty().set(false);
		buttonStopOpt.disableProperty().setValue(true);
		textStatus.setText("Idle");
	}
}
