package me.pogorzelski.ekopt;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class App extends Application {

	public static final Logger logger = LogManager.getLogger(Logger.class.getName());

	public static String dataDir = "data";
	public static String binDir = "ek-battlesim";
	public static String arenaDir = "ranked";
	public static String tmpDeckFile = "tmp_deck.txt";
	public static String tmpOutputFile = "tmp_output.txt";
	public static String tmpInputFile = "tmp_input.txt";
	public static String tmpBestOutputFile = "tmp_best_output.txt";
	public static Stage currentStage;

	public static void main(String[] args) throws IOException {
		launch(args);
	}


	@Override
	public void start(Stage stage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("/fxml/main.fxml"));
		Scene scene = new Scene(root);
		scene.getStylesheets().add("styles/main.css");
		stage.setTitle("EK Opt");
		stage.setScene(scene);
		App.currentStage = stage;
		stage.show();
	}
}
