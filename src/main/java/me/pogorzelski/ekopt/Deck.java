package me.pogorzelski.ekopt;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableStringValue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Deck implements Comparable<Deck> {

	public static final Logger logger = LogManager.getLogger(Logger.class
			.getName());

	List<Card> cards = new ArrayList<Card>();
	List<Rune> runes = new ArrayList<Rune>();
	Double mpm = 0.0;
	Integer cost = 0;
	StringProperty output = new SimpleStringProperty();

	public void sort() {
		Collections.sort((List<Card>) cards);
		Collections.sort((List<Rune>) runes);
	}

	public void dumpToTxt(String lvl, Boolean orderImportant, String waitToPlay)
			throws IOException {
		// sort();
		File deckFile = new File(new File(App.dataDir, App.binDir),
				App.tmpDeckFile);
		BufferedWriter writer = new BufferedWriter(new FileWriter(deckFile));
		writer.write("Player name: TMP\n");
		writer.write("Player level: " + lvl + "\n");
		if (orderImportant) {
			writer.write("--skip-shuffle\n");
			writer.write("--wait-to-play=" + waitToPlay + "\n");
		}
		
		writer.write("\n");
		for (Card card : cards) {
			writer.write(card.getDescription() + "\n");
		}
		writer.write("\n");
		for (Rune rune : runes) {
			writer.write(rune.getDescription() + "\n");
		}
		writer.close();
	}

	@Override
	public int compareTo(Deck o) {
		// TODO Auto-generated method stub
		return this.mpm.compareTo(o.mpm);
	}

	@Override
	public int hashCode() {
		int result = cards.hashCode();
		result = 31 * result + runes.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;

		Deck deck = (Deck) obj;

		if (cards.equals(deck.cards) && runes.equals(deck.runes))
			return true;

		return false;
	}

}
