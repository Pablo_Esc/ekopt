EKopt

1. Important notice
This app :
- does not need to enter you EK login / password
- does not contain any viruses, keyloggers etc. I swear :)
I :
- can't be responsible if the results don't match your ideal ones. This app tries but nothing beats human brain.

2. Prerequisites and launching
- You need to have installed Java Runtime Enviroment - latest from the 8 branch . Download available on : http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
- You need to make sure that your system is using this verion of java because you can have multiple versions installed

You have two files besides that readme. One .exe - for Windows, one jar for Linux / Mac. Windows users - doubleclick exe file. Linux/Mac users try to doubleclick the jar file, and if this doesn't work enter java -jar ekopt-0.0.1-SNAPSHOT.jar
On the first run app will download the crystark sim and everytime it's updated it'll pull the update by itself.

3. Usage :

There are 6 options for the app : 
- DI
- EW
- Thief 
- KW
- Arena
- Hydra

3.1 DI
You just need to place your deck in the card pool section, and select Demon below, and name of the Demon further.
Card pool definintion explained later.

3.2 Thief
You just need to place your deck in the card pool section, and select Theif below.
Card pool definintion explained later.

3.3 Hydra
You just need to place your deck in the card pool section, and select Hydra below.
Card pool definintion explained later.

3.4 KW
You just need to place your deck in the card pool section, and select Kingdom War below.
Card pool definintion explained later.

3.5 Arena
It's working just needs to get manual input. Didn't have time to make it auto. If you want to use it. Contact me.

3.6 EW
You just need to place your deck in the card pool section, and select EW below, and input cards that you fight against.
Card pool definintion explained later.

4. Card pool definion

You'd better prepare some text file on the side, I keep one for each demon / event. These files will be a definition of your card pool - cards you figure can be important to this sim.
You insert cards like this :
Leprechaun, 15, Prayer:280
Card name, enchant lvl, Evo skill with it's strenght

Leprechaun, 11+5, Prayer:120
means it's Leprechaun card, enchanted at 11 lvl, but with 5th evolution - Prayer 120.

You enter cards line by line and then you make a break and enter runes you want to consider.
Example :
Leprechaun, 15, Prayer:280
Vulcan, 10+5, Immunity

Blight Stone
Red Valley

Don't overdo with number of cards. Pick the ones you feel are the most needed. 
!!!! You need to be precise with card, skill names !!!!!! In case of doubt you can always check data/ek-battlesim/cards.supported.txt file which covers all the game cards.

Important :
If you're sure that you want to include some card, rune in your deck like Butcher in DT etc. just prepend the card name with ! like :
!Butcher, 15, Craze 70

For EW / Hydra :
Merit cards are prepended with E: like :
E:Chaos Demon, 15, Trap:2

For EW opposing deck must have LG: in front of all cards names like :
LG: Jackfrost (Legendary), 15

5. Settings
Second tab consists of settings :
- Player lvl : self explanationary
- Random / Best / Runes iterations : 
App works in 3 phases : 
- first it picks randomly cards
- second it tries to picks the best cards
- third it tries to settle the runes
These settings name how many sims in each group should be made. 
It all depends on your PC computing power but on my old PC i have : 
	Random : 50
	Best : 20000
	Runes : 50
- minimal cards to pick : self explanationary, just don't set it at 10 (some bug I need to work around :) )
- internal iterations : this value is passed to crystark for it's sim. Setting is all dependant on you PC power, but if you set it high the sim will be veeeeeery slow.
I have mine at : 
	- DI, Hydra, EW : 2500
	- Arena : 25
	- KW : 250

- path to java executable : you probably won't have to insert there anything
- check for update : self explanationary
- keep deck according to lvl cost : self explanationary
- wait to play : only for EW - between 2-5 is a setting that makes sim wait until you have this number of cards in hand before releasing them on deck


Enjoy, and in case of problems you know where to find me :)